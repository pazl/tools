# tools

Simple tools that don't fit into their own repository.

## wkd-get-key.rb

Fetch an OpenPGP-key from a Web Key Directory and save it to a file.
