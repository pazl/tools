#!/usr/bin/env ruby

ENV["GEM_HOME"] = Gem.user_dir

require 'bundler/inline'

gemfile(false) do 
  source 'https://rubygems.org'
  gem 'base32'
end

# stdlib
require 'net/http'
require 'digest'
require 'open-uri'
require 'resolv'

ADVANCED_URI = "https://openpgpkey.__DOMAIN__/.well-known/openpgpkey/__DOMAIN__/hu"
DIRECT_URI = "https://__DOMAIN__/.well-known/openpgpkey/hu"

def wkd_hash(string)
  # Table for z-base-32 encoding.
  Base32.table = "ybndrfg8ejkmcpqxot1uwisza345h769"
  Base32.encode(Digest::SHA1.digest(string.downcase))
end

def supports_advanced_method?(domain)
  Resolv.getaddress("openpgpkey.#{domain}")
  true
rescue Resolv::ResolvError
  false
end

def advanced_uri(domain, hash)
  if supports_advanced_method?(domain)
    File.join(ADVANCED_URI.gsub('__DOMAIN__', domain), hash)
  else
    false
  end
end

def direct_uri(domain, hash)
  File.join(DIRECT_URI.gsub('__DOMAIN__', domain), hash)
end


case ARGV.first
when nil, "", "-h", "--help"
  puts "Usage: #{__FILE__} EMAILADDRESS"
  exit 1
end

emailaddr = ARGV.first
local_part, domain = emailaddr.split('@', 2)
hash = wkd_hash(local_part)
uri = advanced_uri(domain, hash) || direct_uri(domain, hash)

begin
  keymaterial = URI.parse(uri).read
rescue OpenURI::HTTPError => exc
  puts exc
  exit 1
end

filepath = "#{emailaddr}.pgp"
File.open(filepath, 'w') { |fh| fh.write(keymaterial) }
puts "Success! Key written to #{filepath}"

